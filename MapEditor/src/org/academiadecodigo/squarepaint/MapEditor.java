package org.academiadecodigo.squarepaint;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.squarepaint.color.PositionColor;
import org.academiadecodigo.squarepaint.utils.FileManager;

public class MapEditor {

    private Grid grid;
    private GreenCursor cursor;
    private boolean painting;
    private PositionColor color;

    public MapEditor(int rows, int cols){

        grid = new Grid(rows, cols);
        cursor = new GreenCursor();
        this.color = PositionColor.BLACK;


    }

    public void moveCursor(GreenCursor.Direction direction ){
        if(cursorOnEdge(direction)){
            return;
        }
        switch (direction){

            case UP:
                cursor.moveUp();
                break;
            case DOWN:
                cursor.moveDown();
                break;
            case RIGHT:
                cursor.moveRight();
                break;
            case LEFT:
                cursor.moveLeft();
                break;

        }
        if(painting) {
            paintPosition();
        }

    }

    public void paintPosition(){
        Position position = grid.getPosition(cursor.getRow(),cursor.getCol());
        if(position.isPainted()){

            position.erase();
            position.setColor(Color.BLACK);
        } else {
            position.setColor(color.getCurrentColor().getSimpleGfxColor());
            position.paint();
        }
    }


    public void setColor() {
        color.nextColor();
    }

    public void clear(){
        grid.clear();


    }

    public void load() {
        grid.stringToGrid(FileManager.readFile());
    }

    public void save() {
        FileManager.writeToFile(grid.toString());
    }

    public void setPainting(boolean painting){
        this.painting = painting;
    }

    private boolean cursorOnEdge(GreenCursor.Direction direction){
        return direction == GreenCursor.Direction.UP && cursor.getRow() == 0 ||
                direction == GreenCursor.Direction.DOWN && cursor.getRow() == grid.getRows() -1 ||
                direction == GreenCursor.Direction.LEFT && cursor.getCol() == 0 ||
                direction == GreenCursor.Direction.RIGHT && cursor.getCol() == grid.getCols() -1;
    }

}

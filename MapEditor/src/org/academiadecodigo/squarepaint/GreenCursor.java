package org.academiadecodigo.squarepaint;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import static org.academiadecodigo.squarepaint.Grid.CELLSIZE;
import static org.academiadecodigo.squarepaint.Grid.PADDING;



public class GreenCursor extends Position {


    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public GreenCursor() {
        super(0, 0);
        rectangle.setColor(Color.GREEN);
        paint();
    }

    public void moveUp(){
        rectangle.translate(0, -CELLSIZE);
        this.row--;
    }

    public void moveDown(){
        rectangle.translate(0, CELLSIZE);
        this.row++;
    }

    public void moveRight(){
        rectangle.translate(CELLSIZE, 0);
        this.col++;
    }

    public void moveLeft(){
        rectangle.translate(-CELLSIZE, 0);
        this.col--;
    }



}

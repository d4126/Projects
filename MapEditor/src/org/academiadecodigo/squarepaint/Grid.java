package org.academiadecodigo.squarepaint;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Grid {

    public static int PADDING = 10;
    private int cols;
    private int rows;
    public static int CELLSIZE = 20;
    private Position[][] positions;



    public Grid(int rows, int cols){
        this.rows = rows;
        this.cols = cols;

        positions = new Position[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                positions[row][col] = new Position(row,col);
            }
        }
    }

    public void stringToGrid(String grid) {

        int index = 0;

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {

                Position position = positions[row][col];

                if (grid.charAt(index) == '0') {
                    position.erase();
                } else {
                    position.paint();
                }
                index++;
            }
            index++;
        }

    }
    public void clear() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                positions[row][col].erase();
            }
        }
    }

    public Position getPosition(int row, int col){
        return positions[row][col];
    }

    public int getRows() {
        return rows;
    }

    public int getCols(){
        return cols;
    }

    public String toString(){

        StringBuilder stringBuilder = new StringBuilder();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                stringBuilder.append(positions[row][col]);
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

}

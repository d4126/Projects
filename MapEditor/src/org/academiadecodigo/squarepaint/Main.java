package org.academiadecodigo.squarepaint;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;

import java.awt.*;
import java.util.Map;

public class Main {

    public static void main(String[] args) {


        MapEditor map = new MapEditor(20,20);
        new KeyboardListener(map);
    }

}

package org.academiadecodigo.squarepaint.color;

import org.academiadecodigo.simplegraphics.graphics.Color;

public enum PositionColor {

    BLACK(Color.BLACK),
    BLUE(Color.BLUE),
    YELLOW(Color.YELLOW),
    ORANGE(Color.ORANGE),
    RED(Color.RED);

    private Color simpleGfxColor;
    private int index;


    PositionColor(Color simpleGfxColor) {
        this.simpleGfxColor = simpleGfxColor;
        this.index = 0;

    }


    public Color getSimpleGfxColor() {
        return simpleGfxColor;
    }

    public PositionColor getCurrentColor() {
        return PositionColor.values()[index];
    }

    public PositionColor nextColor() {
        if (index < PositionColor.values().length -1) {
            index += 1;
            System.out.println(index);
            return PositionColor.values()[index];
        }
        System.out.println(index);
        index = 0;
        return PositionColor.values()[index];
    }

}

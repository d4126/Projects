package org.academiadecodigo.squarepaint;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.squarepaint.color.PositionColor;

import static org.academiadecodigo.squarepaint.Grid.CELLSIZE;
import static org.academiadecodigo.squarepaint.Grid.PADDING;


public class Position {
    protected int row;
    protected int col;
    Rectangle rectangle;
    private boolean painted;
    private PositionColor color;

    public Position(int row, int col){
        this.col = col;
        this.row = row;
        rectangle = new Rectangle(col * CELLSIZE + PADDING,row * CELLSIZE + PADDING,CELLSIZE,CELLSIZE);
        rectangle.draw();
        this.color = PositionColor.BLACK;


    }

    public void paint(){
        painted = true;

        rectangle.fill();
    }

    public void setColor(Color color) {
        rectangle.setColor(color);
    }

    public void erase(){

        painted = false;
        rectangle.draw();



    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public boolean isPainted() {
        return painted;
    }





    @Override
    public String toString() {
        return painted ? String.valueOf(color.getCurrentColor().ordinal()) : "0";
    }
}

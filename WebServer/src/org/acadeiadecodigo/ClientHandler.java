package org.acadeiadecodigo;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientHandler implements Runnable{

    private static Logger logger = Logger.getLogger(ClientHandler.class.getName());


    private final Socket clientSocket;

    public ClientHandler(Socket socket)
    {
        this.clientSocket = socket;
    }

    @Override
    public void run() {
        
        try {

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());

            String requestHeaders = fetchRequestHeaders(in);
            if(requestHeaders.isEmpty()) {
                clientSocket.close();
                return;
            }

            String request = requestHeaders.split("\n")[0]; // request is first line of header
            String httpVerb = request.split(" ")[0]; // verb is the first word of request
            String resource = request.split(" ").length > 1 ? request.split(" ")[1] : null; // second word of request

            System.out.println(request);

            logger.log(Level.INFO, "Request received: " + request);
            logger.log(Level.FINE, "Headers : \n" + requestHeaders);

            if (!httpVerb.equals("GET")) {
                logger.log(Level.WARNING, "resource not specified from " + clientSocket.getInetAddress());
                out.writeBytes(HttpHelper.notAllowed());
                clientSocket.close();
                return;
            }
            if (resource == null) {
                logger.log(Level.WARNING, "resource not specified from " + clientSocket.getInetAddress());
                out.writeBytes(HttpHelper.badRequest());
                return;
            }

            String filePath = getPathForResource(resource);
            File file = new File(filePath);
            if (file.exists() && !file.isDirectory()) {

                out.writeBytes(HttpHelper.ok());

            } else {

                logger.log(Level.WARNING, file.getPath() + "not found");
                out.writeBytes(HttpHelper.notFound());
                filePath = Server.DOCUMENT_ROOT + "404.html";
                file = new File(filePath);

            }

            out.writeBytes(HttpHelper.contentType(filePath));
            out.writeBytes(HttpHelper.contentLength(file.length()));
            streamFile(out, file);
            clientSocket.close();


        } catch (IOException ex) {

            logger.log(Level.WARNING, ex.getMessage());

        }


    }

    private String fetchRequestHeaders(BufferedReader in) throws IOException {

        String line = null;
        StringBuilder builder = new StringBuilder();

        //read the full http request
        while ((line = in.readLine()) != null && !line.isEmpty()) {
            builder.append(line + "\n");
        }

        return builder.toString();

    }

    private void streamFile(DataOutputStream out, File file) throws IOException {

        byte[] buffer = new byte[1024];
        FileInputStream in = new FileInputStream(file);

        int numBytes;
        while ((numBytes = in.read(buffer)) != -1) {

            out.write(buffer,0,numBytes);

        }

        in.close();

    }

    private String getPathForResource(String resource) {

        String filePath = resource;

        Pattern pattern = Pattern.compile("(\\.[^.]+)$");
        Matcher matcher = pattern.matcher(filePath);

        if(!matcher.find()) {
            filePath += "/index.html";

        }

        filePath = Server.DOCUMENT_ROOT + filePath;

        return filePath;

    }


}

package org.acadeiadecodigo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server {

    private static Logger logger = Logger.getLogger(Server.class.getName());

    public static final String DOCUMENT_ROOT = "www/";
    public static final int DEFAULT_PORT = 8080;

    private ServerSocket bindSocket = null;

    public static void main(String[] args) {

        Server server = new Server();
        server.listen(DEFAULT_PORT);

    }

    private void listen(int port) {

        try {

            bindSocket = new ServerSocket(port);
            logger.log(Level.INFO, "server bind to " + bindSocket.getInetAddress());

            serve(bindSocket);
        } catch (IOException e) {

            logger.log(Level.SEVERE, "could not bind to port" + port);
            logger.log(Level.SEVERE, e.getMessage());
            System.exit(1);

        }
    }

    private void serve (ServerSocket bindSocket){

        while (true){

            try {

                Socket clientSocket = bindSocket.accept();
                logger.log(Level.INFO, "new connection from " + clientSocket.getInetAddress());

                ClientHandler clientSock = new ClientHandler(clientSocket);
                ExecutorService cachedPool = Executors.newCachedThreadPool();


                cachedPool.submit(clientSock);





            } catch (IOException e){

                logger.log(Level.SEVERE, e.getMessage());

            }
        }
    }
}


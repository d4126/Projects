const relogio = document.querySelector('.relogio');

const tick = () => {

    const now = new Date();

    const h = dateFns.format(now, 'H');
    const m = dateFns.format(now, 'mm');
    const s = dateFns.format(now, 'ss');

    const html= `
    <span>${h}</span> :
    <span>${m}</span> :
    <span>${s}</span>  
    `;

    relogio.innerHTML = html;
};

setInterval(tick, 1000)